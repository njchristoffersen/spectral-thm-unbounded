\documentclass[letterpaper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb, amsthm}
\usepackage{xcolor}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{proposition}[theorem]{Proposition}

\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}



\begin{document}
    \title{Spectral Theorem for Self-Adjoint Unbounded Operators}
    \author{Nicholas Christoffersen}
    \date{November 19, 2021}

    \maketitle

    \section{Introduction}
    \subsection{Abstract}

    The goal of this piece is to explain the background of the Spectral Theorem for self-adjoint (possibly unbounded) operators. I want to begin the section with some motivation, given from the situation in the finite-dimensional case, and then using an example in the unbounded self-adjoint case to serve as motivation for the general theorem.

    \subsection{Matrix Case}

    We now recall the situation in the case of finite-dimensional self-adjoint linear operators. Let $V$ be a finite-dimensional Hilbert space, and let $A: V \to V$ be a self-adjoint operator. Then we know from linear algebra that $A$ is orthogonally diagonalizable. That is: if $\lambda_1 < \lambda_2 < \ldots < \lambda_k$ are the eigenvalues of $A$, and $E_{\lambda_1}, \ldots, E_{\lambda_k}$ are the associated eigenspaces, then
    \begin{align*}
        V &\cong E_{\lambda_1} \oplus E_{\lambda_2} \oplus \ldots \oplus E_{\lambda_k}\\
        A &= \sum_{i = 1}^{k} \lambda_i P_{E_{\lambda_i}},
    \end{align*}
    where $P_{E}$ denotes the orthogonal projection onto the closed subspace $E$.

    We want to connect this to the spectral theorem, for which we will assume the following version for bounded \textit{normal} operators:

    \begin{theorem}[Spectral Theorem for Bounded Normal Operators \cite{Hall2013:QuantumForMath}]\label{thm:spec_normal}
        Suppose $A\in \mathcal{B}(H)$ is normal. Then there exists a unique projection-valued measure $\mu^{A}$ on the Borel $\sigma$-algebra in $\sigma(A)$, with values in $\mathcal{B}(H)$, such that
        \[
            \int_{\sigma(A)}^{}  \lambda d \mu^{A}(\lambda) = A.
        \]
        Furthermore, for any measurable set $E\subseteq \sigma(A)$, $\operatorname{Range}(\mu^{A}(E)$ is invariant under $A$ and $A^{*}$.
    \end{theorem}

    In the finite-dimensional, self-adjoint case described above, the spectral measure from the theorem is defined by setting $\mu^{A}(-\infty, \lambda]$ to be the projection onto  $\{\oplus_{\lambda_i \leq \lambda}^{} E_{\lambda_i}\}$. Then we have that $d \mu(\lambda) = P_{E_\lambda}$ if $\lambda$ is an eigenvalue, and $d \mu (\lambda) = 0$ otherwise. Thus, we have that by the theorem:
    \[
        A = \int_{\sigma(A)}^{}  \lambda d \mu^{A}(\lambda) = \sum_{i = 1}^{k} \lambda_i P_{E_{\lambda_{i}}},
    \]
    which is exactly the statement we arrived to in linear algebra. 


    \subsection{Example: The Multiplication Operator}

    Now, we wish to generalize the preceeding statements. In moving to the infinite dimensional world, we need to address the questions of unbounded linear operators, direct integrals, strong convergence, and more. We begin with an example. For the reader's clarity, we will provide general results alongside the motivating example. To separate the general results from statements of the specific operator, we will let $T$ denote some operator defined in a Hilbert space $H$, and will denote our specific example as follows:

    Consider the Hilbert space $L^2(\R)$ of square-integrable functions $f:\R \to \C$. Define the operator $A$ as follows:
    \[
        (Af)(x) = xf(x).
    \]
    Note that $A$ is not everywhere defined. In particular, consider the function $g:\R \to \C$ defined by 
    \[
        g(x) =
        \begin{cases}
            \frac{1}{x} & \text{if }x \geq 1\\
            0 & \text{otherwise}.
        \end{cases}
    \]
    Indeed, $g\in L^2(\R)$, however, we see that $Ag\not\in L^2(\R)$ since
    \[
        \left\|Ag\right\|^2 = \int_{1}^{\infty} 1 dx = +\infty.
    \]
    Thus, one of the issues we encounter when moving to the infinite dimensional case is that linear operators need not be everywhere defined. Thus we often say we have an operator $T$ defined \textit{in} (rather than \textit{on}) a Hilbert space $H$, and we must specify its domain, $D(T)$. In this case, we have
    \[
        D(A) = \{f\in L^2(\R) : \int_{\R}^{} |xf(x)|^2 dx < \infty \}.
    \]

    We will see shortly that we focus our attention to operators that are \textit{densely defined}. That is, operators $T$ such that $D(T)$ is dense in $H$. In our example, we note that $C_{0}(\R)\subseteq D(A)$, so that indeed $A$ is densely defined ($C_0(\R)$ denotes the functions of compact support, which are dense in $L^2(\R)$).

    Now, in keeping with the flow of the matrix case, we want to check that $A$ is \textit{self-adjoint}. This too is more nuanced in the infinite-dimensional case. We give the following defintion:
    \begin{definition}\label{def:symmetric}
        Let $T$ be an operator in $H$. Then we say that $T$ is \textit{symmetric} if for all $x, y\in D(T)$,
        \[
            \left<x, Ty \right> = \left<Tx, y \right>.
        \]
    \end{definition}
    The question is, when are we able to define an adjoint? In general, the operator $T$ need not be everywhere defined, and so neither is $T^{*}$. To this end, suppose that for some $\phi\in H$, we have that the map $\phi \mapsto \left<\psi, T\phi \right>$ is a bounded linear map from $D(T)$ to $\C$. Then since $D(T)$ is dense in $H$ and this map is bounded, it can be extended to a bounded map on all of $H$. Thus, by the Riez Representation Theorem, we have that there exists a $\eta \in H$ such that 
    \[
        \left<\psi, T\phi \right> = \left<\eta, \phi \right>
    \]
    for all $\phi\in D(A)$. This motivates the following definition:
    \begin{definition}
        Let $T$ be an operator in $H$. Then we denote
        \[
            D(T^{*}) = \left\{ \psi\in H \mid \phi \mapsto \left<\psi, T\phi \right> \text{ is a bounded, linear functional on } D(T)\right\},
        \]
        and for $\psi \in D(T^{*})$, we define $T^{*}\psi$ to be the unique vector $\eta$ such that $\left<\psi, T\phi \right> = \left<\eta, \phi \right>$ for all $\phi\in D(T)$.
    \end{definition}

    \begin{definition}
        We say an operator $T$ in $H$ is self-adjoint if $D(T) = D(T^{*})$ and for all $\phi\in D(T)$, the two operators coincide: $T\phi = T^{*}\phi$. 

        In general, equality of operators in this general Hilbert space setting requires equality of domains.
    \end{definition}

    Now, back to our example. The operator $A$ is symmetric: for all $f,g \in D(A)$, we have that
    \[
        \left<f, Ag \right> = \int_{\R}^{} f(x) \overline{xg(x)}dx = \int_{\R}^{} xf(x)\overline{g(x)}dx = \left<Af, g \right>.
    \]
    Now, note that since $A$ is symmetric, we get that $D(A)\subseteq D(A^{*})$. To show equality of the domains, suppose that $g\in D(A^{*})\setminus D(A)$. Then by dominated convergence, and that $A$ is defined on compactly supported functions, that for each $n\in \N$, we have 
    \[
        g\chi_{[-n,n]}\in D(A) \text{ and } \left\|A(g\chi_{[-n,n]})\right\| \nearrow \left\|Ag\right\| = +\infty.
    \]
    Then, denoting $\phi_{n}(x) = xg(x) \chi_{[-n,n]}(x)$, we have that
    \[
        \left<g, A\phi_n \right> = \int_{-n}^{n} \left|xg(x)\right|^2 dx = \left\|\phi_n\right\|^2.
    \]
    However, this shows that the map:
    \[
        \phi \mapsto \left<g, A\phi \right>
    \]
    is not a bounded map, since $\left<g, A(\phi_n / \left\|\phi_n\right\|) \right> = \left\|\phi_n\right\| \nearrow \infty$. This contradicts the fact that $g\in D(A^{*})$. Thus, $D(A) = D(A^{*})$ and since $A$ is symmetric, this shows that $A$ is self-adjoint. Further, we can see that $\sigma(A) = \R$, since for $\lambda\in \R$, we have that $A - \lambda I$, we have that the functions $\psi_n$ defined by
    \[
        \psi_n = n \chi_{[\lambda - \frac{1}{2n}, \lambda + \frac{1}{2n}]}
    \]
    are approximate eigenvectors for $A$ associated to the eigenvalue $\lambda$. That is, $\left\|\psi_n\right\| = 1$ but
    \[
        (A - \lambda I)\psi_n \to 0.
    \]
    This shows that $\R\subseteq \sigma(A)$. To show the reverse inclusion, we use the fact (without proof) that self-adjoint operators have strictly real spectrum. An interesting exercise is to see that there exist symmetric, densely-defined operators with non-real spectrum.

    Now, we show the Heuristic behind the statment of the theorem. Notice that for each $\phi\in L^2(\R)$ with $\operatorname{supp}(\phi)\subseteq [\lambda , \lambda + \epsilon]$, we have that
    \[
        \left\|(A - \lambda I)\phi\right\|^2 \leq \int_{\lambda}^{\lambda + \epsilon} |(x - \lambda)\phi(x)| ^2 dx \leq \epsilon^2 \int_{\R}^{} |\phi|^2 = \epsilon^2 \left\|\phi\right\|^2 
    \]
    In other words, we have that $A \approx \lambda I$ on the set of functions supported on $[\lambda, \lambda + \epsilon]$. Thus, if we partitioned the real line into countably many intervals, each containing some point $\lambda_i$, then we would be able to approximate $A \approx \sum_{i}^{} \lambda_i P_i$, where $P_i$ denotes the restriction of functions to those supported on the  $i^{\text{th}}$ interval. We use this to motivate the following:
    %\[
    %    \left\|A\phi\right\|^2 = \int_{\R}^{} |x\phi(x)|^2 dx \leq \max \{|\lambda| \pm \epsilon \}^2\int_{\R}^{}  |\phi(x)|^2dx = \max \{|\lambda| \pm \epsilon\}^2 \left\|\phi\right\|^2,
    %\]
    %and similarly,
    %\[
    %    \left\|A\phi\right\|^2 \geq \min \{ |\lambda| \pm \epsilon \}^2 \left\|\phi\right\|^2.
    %\]
    %Therefore, we note that for such a $\phi$, we get
    %\[
    %    \left\|(A - \lambda I)\phi\right\| \leq \epsilon \left\|\phi\right\|.
    %\]
    %
    %This allows us to make the following claim:
    \begin{claim}
        For each $n\in \N$ and $\lambda\in \Z / 2^{n}$, define $E_\lambda^{n} = [\lambda, \lambda + \frac{1}{2^{n}}]$ and denote $P_{E_{\lambda}^{n}}$ to be the orthogonal projection onto the set of functions with support contained in $E_{\lambda}^{n}$. Define
        \[
            A_n\phi = \sum_{\lambda\in \Z / 2^{n}}^{} \lambda P_{E_{\lambda}^{n}}
        \]
        with $D(A_n) = D(A)$ for each $n\in \N$. Then $A_n$ is well-defined and $A_n \to A$ in the strong operator topology generated by $D(A)$. That is, for each $\phi\in D(A)$, we have that
        \[
            A_n \phi \to A \phi.
        \]
    \end{claim}
    \begin{proof}
        First, we show that $A_n$ is defined on $D(A)$ for each $n$. For each $\phi\in D(A)$, we have
        \begin{align*}
            \left\|(A_n - A)\phi\right\|^2
            = \sum_{\lambda\in \Z / 2^{n}}^{} \int_{\lambda}^{\lambda + \frac{1}{2^{n}}} |\lambda - x|^2 |\phi(x)|^2 dx
            \leq \frac{1}{2^{2n}} \left\|\phi\right\|^2
        \end{align*}
        so that $\left\|(A_n - A)\phi\right\| < \frac{1}{2^{n}}\left\|\phi\right\|$. Therefore, we have
        \begin{align*}
            \left\|A_n \phi\right\| \leq \left\|(A_n - A)\phi\right\| + \left\|A\phi\right\| \leq \frac{1}{2^{n}}\left\|\phi\right\| + \left\|A\phi\right\| < \infty,
        \end{align*}
        so indeed, we may define $A_n$ on $D(A)$ for each $n\in \N$. Further, using these estimates we have that
        \[
            \left\|(A_n - A)\phi\right\| \to 0,
        \]
        as claimed.
    \end{proof}

    We now remark that the $A_n$ represents some type of approximation to the claimed integral:
    \[
        \int_{\sigma(A) = \R}^{} \lambda dE_\lambda.
    \]
    In this case, the resolution of the identity $E_\lambda$ one can think of as the projection onto the set of functions with support in $(-\infty, \lambda]$. Then, thinking of $P_{E_\lambda^{n}}$ as the approximation $\Delta E_\lambda = E_{\lambda + \frac{1}{2^{n}}} - E_{\lambda}$ of $d E_\lambda$, we may get some intiuition for how the spectral theorem works in infinite dimensions. 

    %\subsection{Infinite-Dimensional Preliminaries}

    %Let $H$ be a (possibly infinite dimensional) Hilbert space, and $A$ be a linear operator defined on some dense subspace of $H$, denoted $D(A)$. We would like to define the adjoint of $A$, but since $A$ may not be everywhere defined, we cannot expect $A^*$ to be everywhere defined, so we define the domain of $A^{*}$ to be
    %\[
    %    D(A^{*}) = \left\{ x\in H  \mid  y \mapsto \left<x, Ay \right> \text{ is a bounded, linear functional on } D(A) \right\}
    %\]

    %Then, for all $x\in D(A^{*})$, we define $A^{*}$ of $H$ to be $\phi$, where $\phi$ is the unique element in $H$ such that $\left<x, Ay \right> = \left< \phi, y \right>$ for all $y\in D(A)$. Such an element exists since the map $B_x:D(A) \to \C$ given by $B_x(y) = \left<x, Ay \right>$ extends to a functional $B_x: H \to \C$ (since it is bounded), and therefore, we can apply the Riez Representation Theorem.

    %Then, we say that two operators $A$ and $B$ are equal if $D(A) = D(B)$ and for all $x\in D(A)$, $Ax = Bx$. In particular, an operator may be symmetric, but not self-adjoint.

    %Then, we may ask a few natural questions. For example, what are the eigenvalues of a symmetric operator? What are the eigenvalues of a self-adjoint operator?


    %Note that in the case of bounded operators, symmetric and self-adjoint operators coincide in a natural way. That is, if we have a bounded operator $A$ on some dense subspace $D(A) \subseteq  H$, then we may extend $A$ to all of $H$. Similarly, $A^{*}$ will extend to all of $H$, and indeed they will coincide on all of $H$ if they coincide on a dense subspace. This is not, however, true of unbounded operators. We will try to extend this idea of 'self-adjoint in a natural way' using the terminology of closed operators and essential self-adjointness.

    %\begin{definition}\label{def:approx_eigenvalue}
    %    We say that $\lambda\in \C$ is an \textit{approximate eigenvalue} of $A$ in $H$ if there exists a sequence $\left( \psi_n \right)_{n\in \N}\subseteq D(A)$ with
    %    \[
    %        \lim_{n \to \infty} (A - \lambda I) \psi_n = 0, \text{ and } \left\|\psi_n\right\| = 1 \text{ for all } n\in \N
    %    \]
    %\end{definition}

    %\begin{definition}
    %    Let $A$ be an operator in $H$. Then we say that $A$ is \textit{closed} if the graph of the operator:
    %    \[
    %        \operatorname{Gr}(A) = \{(x, Ax)  \mid x\in D(A)\}
    %    \]
    %    is closed in the product topology on $H\times H$.
    %\end{definition}

    %With this in mind, we say that $A$ is closable if:

    %And then we say that $A$ is essentially self adjoint if the closure of $A$ is self-adjoint. 

    \section{Main Result}

    In this section, using the previous result as motivation, we give an outline for the proof of the Spectral Theorem for Self-Adjoint Operators. We introduce the \textit{Caley Transform}, $C: \R\to \C$:
    \[
        C(x) = \frac{x + i}{x - i}.
    \]
    The Caley Transform embeds $\R$ onto the unit circle minus the point at 1, $S^{1}\setminus \{1\} \subseteq \C$. The inverse of the transform is given by 
    \[
        D(z) = i \frac{z + 1}{z - 1}.
    \]

    The outline of the proof goes as follows. Let $T$ be a densely-defined, self-adjoint operator. Then the operator given by $C(T)$ in a literal sense is well-defined, and it is normal. Then we may apply Theorem~\ref{thm:spec_normal}. Then, for any function $f$ defined on the spectrum of $T$, we will define:
    \[
        f(T) = f(D(C(T)) = \int_{\sigma(C(T))}^{}  f(D(T)) d\mu^{C(T)}.
    \]
    In other words, we will define a new projection valued measure as $\mu^{T}(E) = \mu^{C(T)}(C(E))$, i.e. $\mu^{T}$ is the pullback of $\mu^{C(T)}$ by $D = C^{-1}$. 

    At first glance, this sketch of a proof may seem benign, but it hinges on the fact that we can make sense of $C(T)$ \textit{without} using the functional calculus that we wish to define. This is what I will explain in this section.

    \subsection{What \textit{is} C(T)?}

    When we define $C(T)$, we mean the operator given by
    \[
        (T + iI)(T - iI)^{-1}.
    \]
    To make sense of this, we need to show that $T - iI$ maps $D(T)$ onto all of $H$, and is injective. Then, we will be able to define a set-theoretic inverse, as seen above. Then, since  $(T - iI)^{-1}\phi\in D(T)$, the next operation as above is well-defined, and again will map onto all of $H$ by the same argument as before. This will allow us to show that indeed, $C(T)$ is unitary. In particular, it is normal.

    Let $\phi\in D(T)$. Then for $\phi \neq 0$, we have
    \begin{align*}
        \left\|(T - iI)\phi\right\|^2 
        &= \left<(T - iI)\phi, (T - iI)\phi \right>\\
        &= \left<T\phi, T\phi \right> - \left<T\phi, i\phi \right> - \left<i\phi, T\phi\right> + \left<i\phi, i\phi \right>\\
        &= \left\|T\phi\right\|^2 + \left\|i\phi\right\|^2 > 0
    .\end{align*}
    Thus, $T - iI$ is injective (on $D(T)$). The same argument shows that $T + iI$ is injective. Now, we show that $(T - iI)(D(T)) = H$. For this, we use the following fact:

    \begin{proposition}
        For an unbounded operator $T$, we have that
        \[
            \operatorname{Range}(T)^{\perp} = \ker (T^{*}).
        \]
    \end{proposition}

    Applying the above theorem to $T - iI$, we get that:
    \[
        \operatorname{Range}(T - iI)^{\perp} = \ker ((T - iI)^{*}) = \ker (T^{*} + iI) = 0,
    \]
    since $T = T^{*}$, and $T + iI$ is injective. Thus, we have that $T - iI$ has dense range. To show that the range is all of $H$ we need the following definition and proposition:

    \begin{definition}
        We say that an operator $T$ in $H$ is \textit{closed} if its graph,
        \[
            \operatorname{Gr}(T) = \{(x, Tx)  \mid x\in D(T)\} \subseteq H\times H
        \]
        is closed (where $H\times H$ is given the product topology).
    \end{definition}

    \begin{proposition}
        If $T$ is a closed operator and $\lambda\in \C$. If $T - \lambda I$ is bounded from below (i.e. there exists $\epsilon > 0$ such that $\left\|T\phi\right\| \geq \epsilon \left\|\phi\right\|$ for all $\phi\in D(A)$, then $\operatorname{Range}(T)$ is closed.
    \end{proposition}

    \textit{Note: The proof of this is somewhat delicate, as you need to ensure that $T - \lambda I$ is also a self-adjoint operator. While it seems trivial, it is not true in general that $(T + S)^{*} = T^{*} + S^{*}$.}

    Further, we have that
    \begin{proposition}
        Self-adjoint operators are closed.
    \end{proposition}

    I will fill in proofs for these statements soon (by end of the semester), but for now we will assume them without proof.

    Using these, we have exactly that the range of $T - iI$ is all of $H$, and the same is true for $T + iI$. Combining these statements together, we get that the operator defined by
    \[
        \phi \mapsto (A + iI)(A - iI)^{-1}\phi
    \]
    is an operator defined on all of $H$. Further, we have that
    \[
        \left<(A - iI)\phi, (A - iI)\phi \right> = \left\|A\phi\right\| + \left\|\phi\right\| = \left<(A + iI)\phi, (A + iI)\phi \right>,
    \]
    for all $\phi\in D(A)$. Therefore, we have that
    \begin{align*}
        &\left<(A + iI)(A - iI)^{-1}\psi, (A + iI)(A - iI)^{-1}\psi \right>\\
        &=\left<(A - iI)(A - iI)^{-1}\psi, (A - iI)(A - iI)^{-1}\psi \right>\\
        &= \left<\psi, \psi \right>
    .\end{align*}

    Thus, we get that $U := C(A)$ is bijective, preserves norms, and is therefore unitary. More details are needed to show that indeed $D(U)$ (defined using the Spectral Theorem for Bounded, Normal Operators) gives us $A$, as desired. 

    \bibliographystyle{alpha}
    \bibliography{./localbib.bib}
    
\end{document}
